var studentsAndPoints = ['Алексей Петров', 0,
    'Ирина Овчинникова', 60,
    'Глеб Стукалов', 30,
    'Антон Павлович', 30,
    'Виктория Заровская', 30,
    'Алексей Левенец', 70,
    'Тимур Вамуш', 30,
    'Евгений Прочан', 60,
    'Александр Малов', 0],
    students = [];
// 1.1 Создаем массив
var globalShow = function () {
    console.log("Студент " + this.name + " набрал " + this.point + " баллов");
    };

studentsAndPoints.forEach(function (value, i, arr) {
    if (!(i % 2)){
        students.push(
            {   name : value,
                point : arr[i + 1],
                show : globalShow
            }
        );
    }
});
// 2 Добавить в список студентов «Николай Фролов» и «Олег Боровой».
students.push(
    {   name : "Николай Фролов",
        point : 0,
        show : globalShow
    },
    {   name : "Олег Боровой",
        point : 0,
        show : globalShow
    }
);
// console.log(students);
// 3 Увеличить баллы студентам
// «Ирина Овчинникова» и «Александр Малов» на 30,
// а Николаю Фролову на 10.
// students.map(function (obj) {
//     if (obj.name == "Ирина Овчинникова" || obj.name == "Александр Малов") {
//         obj.point += 30;
//         return obj;
//     }else if (obj.name == "Николай Фролов"){
//         obj.point += 10;
//         return obj;
//     }else {
//         return obj;
//     }
//
// }); // Почему вариант с map - не подходит я не понимаю!!!
// Ищем Ирину Овчинникову
students.find(function (obj) {
    return obj.name == "Ирина Овчинникова";
}).point += 30;
// Ищем Александра малова
students.find(function (obj) {
    return obj.name == "Александр Малов";
}).point += 30;
// Ищем Николая Фролова
students.find(function (obj) {
    return obj.name == "Николай Фролов";
}).point += 10;
// 4 Вывести список выше 30 баллов
console.log("4 пункт");
students.forEach(function (obj) {
    if (obj.point >= 30){
        obj.show();
    }
});
// 5 Добавляем поле
// Добавляем поле у объекта.
students.forEach(function (obj) {
    obj.worksAmount = obj.point / 10;
});